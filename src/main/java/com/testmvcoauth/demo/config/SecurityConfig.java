package com.testmvcoauth.demo.config;

import com.testmvcoauth.demo.service.CustomOauth2UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomOauth2UserDetailsService customOauth2UserDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests().antMatchers("/", "/oauth2/**", "/login").permitAll()
                .anyRequest().authenticated().and().logout().logoutUrl("/logout")
                .and().oauth2Login().userInfoEndpoint().userService(customOauth2UserDetailsService)
                .and().defaultSuccessUrl("/welcome");
    }
}
