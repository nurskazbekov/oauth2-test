package com.testmvcoauth.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.Map;

@Controller
public class TestController {


    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/news")
    public String news(Principal principal, Model model) throws JsonProcessingException {

        model.addAttribute("principal", principal.getName());
        return "news";
    }

    @GetMapping("/welcome")
    public String welcome(Principal principal, Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        DefaultOAuth2User defaultOAuth2User = (DefaultOAuth2User) authentication.getPrincipal();

        OAuth2AuthenticationToken oAuth2AuthenticationToken  = (OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        String authorizedClientRegistrationId = oAuth2AuthenticationToken.getAuthorizedClientRegistrationId();

        Map<String, Object> attributes = defaultOAuth2User.getAttributes();

        if (authorizedClientRegistrationId.equals("microsoft")) {
            model.addAttribute("username", attributes.get("preferred_username"));
        } else model.addAttribute("username", attributes.get("name"));
        return "welcome";
    }

    @GetMapping("/logout")
    public String logout() {
        return "redirect:/";
    }
}
