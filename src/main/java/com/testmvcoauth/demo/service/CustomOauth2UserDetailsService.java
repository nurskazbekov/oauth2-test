package com.testmvcoauth.demo.service;

import com.testmvcoauth.demo.model.User;
import com.testmvcoauth.demo.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@Log4j2
@Service
public class CustomOauth2UserDetailsService extends DefaultOAuth2UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(userRequest);

        Map<String, Object> attributes = oAuth2User.getAttributes();

        Collection<? extends GrantedAuthority> authorities = oAuth2User.getAuthorities();

        Optional<User> userOptional = userRepository.findByName((String) attributes.get("name"));

        String provider = userRequest.getClientRegistration().getRegistrationId();

        log.info("USER LOGIN WITH PROVIDER {}", provider);

        if (!userOptional.isPresent()) {
            User user = new User();
            user.setEmail((String) attributes.get("email"));
            user.setName((String) attributes.get("name"));
            user.setProvider(provider);
            userRepository.save(user);
            log.info("user save from {}", user);

        }

        return oAuth2User;
    }
}
